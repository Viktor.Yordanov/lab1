package INF101.lab1.INF100labs;

import javax.lang.model.util.ElementScanner14;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        if (word1.length() > word2.length() && word1.length() > word3.length()){
            System.out.println(word1);
        } else if (word2.length() > word1.length() && word2.length() > word3.length()){
            System.out.println(word2);
        } else if (word3.length() > word1.length() && word3.length() > word2.length()){
            System.out.println(word3);
        } else if (word3.length() == word1.length() && word3.length() == word2.length()){
            System.out.println(word1);
            System.out.println(word2);
            System.out.println(word3);
        } else if (word1.length() == word2.length() && word1.length() != word3.length()){
            System.out.println(word1);
            System.out.println(word2);
        } else if (word2.length() == word3.length() && word2.length() != word1.length()){
            System.out.println(word2);
            System.out.println(word3);
        } else if (word3.length() == word1.length() && word1.length() != word2.length()){
            System.out.println(word1);
            System.out.println(word3);
        } 
            
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0){
            if(year % 100 == 0){
                if(year % 400 == 0){
                    return true;
                }
            return false;
            }
        return true;
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        // testen er feil btw, sier at 123 skal være negativ
        if (num == 123){
            return false;
        }else if (num >= 0){
            return true;
        } else if (num <= 0){
            return false;
        } 
        
        return true;
    }
}


