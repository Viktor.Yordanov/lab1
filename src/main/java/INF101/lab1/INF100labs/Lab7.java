package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        // Creating an Empty Integer ArrayList
        ArrayList<ArrayList<Integer> > aList = 
                  new ArrayList<ArrayList<Integer> >();
  
        // Create 3 lists one by one and append to the 
        // master list (ArrayList of ArrayList)
        ArrayList<Integer> a1 = new ArrayList<Integer>();
        a1.add(3);
        a1.add(0);
        a1.add(9);
        aList.add(a1);
  
        ArrayList<Integer> a2 = new ArrayList<Integer>();
        a2.add(4);
        a2.add(5);
        a2.add(3);
        aList.add(a2);
  
        ArrayList<Integer> a3 = new ArrayList<Integer>();
        a3.add(6);
        a3.add(8);
        a3.add(1);
        aList.add(a3);

        allRowsAndColsAreEqualSum(aList);
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        for (int i = 0; i < grid.size(); i++){
                if(grid.get(i) == grid.get(row)){
                    grid.remove(i);
                }
        }
        return;
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

       ArrayList<Integer> xSums = new ArrayList<>();
       ArrayList<Integer> ySums = new ArrayList<>();

       int x = 0;
       int y = 0;

       // col sum
        while(y < grid.get(0).size()){
            int number = 0;
            for(int i = 0; i < grid.size(); i++){
                number += grid.get(y).get(i); 
            }
            ySums.add(number);
            y +=1;
        }

        // row sum
        while(x < grid.get(0).size()){
            int number = 0;
            for(int i = 0; i < grid.size(); i++){
                number += grid.get(i).get(x); 
            }
            xSums.add(number);
            x +=1;
        }

       
        // Checks
        // System.out.println(xSums);
        // System.out.println(xSums.get(0));
        // System.out.println(xSums.get(1));
        // System.out.println(ySums);
        // System.out.println(ySums.get(0));
        // System.out.println(ySums.get(1));

        if(xSums.get(0) == xSums.get(1) && xSums.get(0) == xSums.get(2)){
            if(ySums.get(0) == ySums.get(1) && ySums.get(0) == ySums.get(2)){
                return true;
            }
            return false;
        } else{
            return false;
        }


        
    }
        


    

}