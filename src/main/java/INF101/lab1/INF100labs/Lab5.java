package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++){
            if(list.contains(3)){
                if(list.get(i).equals(3)){
                    list.remove(i--);
                }
            }
        }
        return list;
    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++){
            for(int j = i+1; j < list.size(); j++){
                if(list.get(i) == list.get(j)){
                    list.remove(j--);
                }
            }
            }
            
        return list;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++){  
            a.set(i, a.get(i) + b.get(i));
        }
        return;
    }

}